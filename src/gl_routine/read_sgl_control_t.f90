!! Timo Subroutine
!! Read in glacier information & glacierized area of all subbasins
       subroutine read_sgl_control_t
       use parm, only:ii_sm_mod,ii_ros,eti_fac,hti_fac_rad_sno,hti_fac_rad_ice,eti_fac_rad,&
                      ros_fac,ros_thr,ii_gm_trig,ii_gm_corr,ii_sf_mod,ii_sf_mod,Tpbase, fac_mm          
       real:: tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10, tmp11, tmp12, tmp13
       real:: test1, test2, test3, test4, test5, test6, test7, test8, test9, test10, test11, test12, test13
       integer:: ierr, cnt
       !real, dimension(:), allocatable:: idum2
       
       skip=1
       
       if (skip==1) THEN
           !! ===============================================================================================================================
           !! a) Read in SWAT-GL Control File
           open(newunit=IU1, file='swatgl_control.txt',status='old',action='read') ! Variante 1
           read(iu1,'(A)') sglcontrol
           ierr=0
           cnt=0
           do while (ierr==0)
                read(iu1, *, iostat=ierr) tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, tmp9, tmp10,tmp11,tmp12,tmp13   
           cnt=cnt+1
           end do
           cnt = cnt - 1
           !allocate(idum2(cnt))
           !allocate(test1(cnt),test2(cnt),test3(cnt),test4(cnt),test5(cnt),test6(cnt),test7(cnt),test8(cnt),test9(cnt),test10(cnt))    
           rewind(iu1)
           read(iu1,'(A)') titldum
           do i=1, cnt       
                !read(iu1, *, iostat=ierr) test1,test2,test3,test4,test5,test6,test7,test8,test9,test10,test11,test12,test13
                read(iu1, *, iostat=ierr) ii_sm_mod,ii_ros,eti_fac,hti_fac_rad_sno,hti_fac_rad_ice,eti_fac_rad, &
                                        ros_fac,ros_thr,ii_gm_trig,ii_gm_corr,ii_sf_mod,Tpbase,fac_mm
           end do
       end if
       return
       end