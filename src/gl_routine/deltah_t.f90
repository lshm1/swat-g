    subroutine deltah_t
    !! Timo Subroutine Delta-h Parametrization 
    
    use parm
    
    real::vv,sum_v,v_control,v_control2,v_control3, esmn
    integer::j,ii
    vv = 0.
    ! Calculate Subbasin Ice thickness Change
    ! Subbasin Loop
    do j = 1,subtot
        v_control=0
        v_control2=0
        v_control3=0
        sum_v=0
        dh=0.
        e_norm=0.
        vv=gl_anmb2(j) ! Glacier Mass Balance Change as Volume (dM in [m�])
        ! Check if Glacier Subbasin
        if (gla_sub2(j)>0) then          
            ! Calculate normalized elevation of each ES
            ! Loop Over ES in that Subbasin
            do ii = 1, numes
                ! Check if Glacier ES
                if (es_gl_a2(ii,j)>0) then
                ! Normalize ES Elevation
                e_norm(ii)=(es_gl_mx_elev(j)-(es_gl_elev(ii,j)-esdist))/(es_gl_mx_elev(j)-(es_gl_mn_elev(j)-(2*esdist)))
                !! Delta-h Empirical Functions based on Huss et al. 2010
                if (gla_sub2(j)>20) then
                    ! Large Valley Glacier >20 km�
                    dh(ii) = (e_norm(ii) - 0.02)**6 + 0.12 *(e_norm(ii)-0.02)                    
                elseif (gla_sub2(j)>5 .AND. gla_sub2(j)<=20)then
                    ! Medium Valley Glacier 5-20 km�
                    dh(ii) = (e_norm(ii) - 0.05)**4 + 0.19 *(e_norm(ii)-0.05) + 0.01
                else
                    ! Small Valley Glacier <5 km�
                    dh(ii) = (e_norm(ii) - 0.3)**2 + 0.6 *(e_norm(ii)-0.3) + 0.09
                end if
                ! Calculate Scaling factor fS 
                ! (positive for accumulation, negative for ablation)
                sum_v=sum_v+(es_gl_a2(ii,j)*dh(ii)*1.0E6) ! Calculate unscaled volume (dh is dimensionless and 10^6 is conversion factor for km� in m�)
                fS=vv/sum_v ! fS in m          
                end if
            end do
  
            ! Update Initial Glacier Thickness 
            do ii = 1, numes
                if (es_gl_a2(ii,j)>0) then
                v_control=v_control + fS * dh(ii)*1000*es_gl_a2(ii,j)*1000
                v_control3=v_control3 + es_gl_wm2(ii,j)*es_gl_a2(ii,j)*1000 ! Control Vol of Subb before ES adaption
                !if (es_gl_m2(ii,j)>10**4) then ! If < 10 m
                es_gl_wm2(ii,j)=es_gl_wm2(ii,j) + (fS * 1000 * dh(ii)) ! fS in m (*1000 for mm)
                es_gl_m2(ii,j)=es_gl_m2(ii,j) + (fS * dh(ii)*1000) ! fS in m (*1000 for mm)
                !if (es_gl_wm2(ii,j)<0.) then ! If GWE gets 0 -> Area = 0 (glacier receedes) 
                if (es_gl_m2(ii,j)<0.) then
                    !es_gl_wm2(ii,j)=0
                    es_gl_a2(ii,j)=0
                    es_gl_m2(ii,j)=0
                    ! Adapt Min Elevation in Case it is necessary 
                    if (es_gl_elev(ii,j)==es_gl_mn_elev(j)) then
                        if (ii<numes) then
                            es_gl_mn_elev(j)=es_gl_elev(ii+1,j)
                        end if
                    end if
                end if
                !v_control2=v_control2 + es_gl_wm2(ii,j)*es_gl_a2(ii,j)*1000 ! Control Vol of Subb after ES adaption
                v_control2=v_control2 + es_gl_m2(ii,j)*es_gl_a2(ii,j)*1000 ! Control Vol of Subb after ES adaption
                end if
                !end if
                ! Write Results to annual summary file (gl_mb_aa.txt)
                if (curyr > nyskip) then
                    !write (89,*) j, ii, iyr, sub_km(j), vv, es_gl_wm2(ii,j), es_gl_a2(ii,j)
                    write (89,8998) j, ii, iyr, sub_km(j), vv, es_gl_m2(ii,j), es_gl_a2(ii,j)
                8998 format (i4,2x,i2,2x,i4,1x,e10.3,1x,e12.4,1x,g12.3,1x,e10.3)                   
                end if
            end do
        end if
    end do
    
    ! --------------------------------
    !! Annual Volume HRU Redistribution 
    !! Initialize Glacier HRUS (HRU Areas & GWEs based on different Scales such as Basin, Total Gl. A, Subbasin etc.)
    call initialize_hrus_t
    
    return
end