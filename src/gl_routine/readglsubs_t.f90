!! Timo Subroutine
!! Read in glacier information & glacierized area of all subbasins
       subroutine readglsubs_t
            
       use parm, only:subtot, hrutot, hru_sub, subs_gl, subs_gla, hru_gl, hru_gla, hru_glww, &
           subs_gl_elsec, subs_gl_mass, subs_gl_el_min, subs_gl_el_max, glsec_num, &
           gl_elsec, gl_elsec_subid, idplt, hru_fr, glw_hru, glw_sub, subs_es_gl_a,& 
           sub_es_cum_gl_a, subs_es_bound,subs_es_gl_ra,subs_es_gl_m,subs_es_gl_wm, &
           subs_es_gl_v,subs_es_gl_rv, gla_sub,glar_sub, glw_sub, glww_sub,glwwr_sub, &
           gla_basin, glww_basin, subs_es_gl_rwm,sub_fr,da_ha,glar_sub2,glwwr_sub2,gla_fr_sub, &
           glww_fr_sub,es_gl_a,es_gl_wm,cpnm,hru_km, gla_sub2,es_gl_a2,es_gl_wm2,glww_sub2, &
           gla_fr_basin,gla_fr_sub2,hru_glww_gltot,hru_glww_basin, sub_km,gla_fr_sub3, es_gl_elev, &
           hru_gl_elev,fractest,fractest2,es_gl_ra,hru_es_id,numes, escheck,hru_glww2,es_glmlt,sub_glmlt2, &
           hru_glww_sub,hru_fr_es,vol_gl1,vol_gl2, iyr,es_gl_mn_elev,es_gl_mx_elev,hedgl,dh,e_norm, &
           glmtmp,glmfmx,glmfmn,f_frze,f_accu,es_gl_m,es_gl_m2,esdist
       !implicit none
       integer::j,k
       integer:: IU, ierr, cnt, cnt2, I, xy, count
       integer:: idum,iglhru,div,divall
       real:: tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, vol1, vol2, vol3, error
       real:: sumcheck, test2, test3,test4,test5, error1, error2,fac_area,teststr
       real, dimension(:,:), allocatable:: test
       real, dimension(:), allocatable:: xx
       integer, dimension(:), allocatable:: ID
       double precision, dimension(:), allocatable:: a,b,c,d,e,x,y,z,ix,idum2
       integer:: interval ! Elevation Section Spacing of Glaciers
       integer:: skip
       character (len=4) :: cropname, count2
       skip=1
       !! Initialize Subbasin Summary Values ( Gl. Area, ES number per Subbasin, Glacier Sub (yes no), & GWE)
       sub_glmlt2=0
       esdist=0
       glsec_num=0
       subs_gl=0
       glw_sub=0
       gla_sub=0
       glar_sub2=0
       glar_sub=0
       glww_sub=0
       glwwr_sub=0
       glwwr_sub2=0
       glww_fr_sub=0
       gla_fr_sub=0
       gla_fr_sub2=0
       gla_fr_sub3=0
       gla_fr_basin=0
       gla_sub2=0
       vol_gl1=0
       vol_gl2=0
       vol3=0
       es_gl_mn_elev=0
       es_gl_mx_elev=0
       glmtmp=0
       glmfmx=0
       glmfmn=0
       f_frze=0
       f_accu=0
       !real:: eb_bounds
       !eb_bound =[2300,2700,3100,3650,max([subs(:).ElevMax])]
    
       if (skip==1) THEN
       !! ===============================================================================================================================
       !! a) Read SWAT-GL Control File
           call read_sgl_control_t
       !! ===============================================================================================================================
       !! b) Read in HRU Glacier Parameters
       open(newunit=IU1, file='gl_hru_par.txt',status='old',action='read') ! Variante 1
       read(iu1,'(A)') glpars
       ierr=0
       cnt=0
       do while (ierr==0)
            read(iu1, *, iostat=ierr) idum, tmp1, tmp2, tmp3, tmp4, tmp5 
       cnt=cnt+1
       end do
       cnt = cnt - 1
       allocate(idum2(cnt))    
       rewind(iu1)
       read(iu1,'(A)') titldum
       do i=1, cnt       
              read(iu1, *, iostat=ierr) idum2(i), glmtmp(i), glmfmx(i),glmfmn(i),f_frze(i),f_accu(i)
       end do
       
       !! ===============================================================================================================================
       !! c) Glacier ES Infos per Subbasin (swat_gles_full.txt)
       open(newunit=IU, file='swat_gles_full.txt',status='old',action='read') ! Variante 1
       read(iu,'(A)') titldum
       ierr = 0
       cnt  = 0
       cnt2=0
       xx = 0
       xy=0
       interval = 100 
        !do
        !  read(iu, *, iostat=ierr) idum, tmp1, tmp2, tmp3   ! 32	58047.95633	15.467	0.03
        !  if (ierr == 0) exit                        ! open (102,file=figfile)
        !  cnt = cnt + 1
        !end do
    
       do while(ierr == 0)
          ! read(iu, *, iostat=ierr) idum, tmp1, tmp2, tmp3   ! 32	58047.95633	15.467	0.03           
           ! read(iu, *, iostat=ierr) idum, tmp1, tmp2, tmp3, tmp4   ! oder 32	15.47	3953	4888	0.03
           read(iu, *, iostat=ierr) idum, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, &
               tmp7, tmp8
           cnt = cnt + 1
       end do
       cnt = cnt - 1 ! Line Number of swat_gles file (glacier subbasins * #ES)
       if(cnt > 0) THEN
           ! allocate(x(cnt),y(cnt),z(cnt),id(cnt))
            allocate(x(cnt),y(cnt),z(cnt),ix(cnt),id(cnt),a(cnt),b(cnt),c(cnt),d(cnt),e(cnt))    
       else 
           error stop 'empty file'
       end if
        !
       rewind(iu)
       read(iu,'(A)') titldum
       ierr = 0
       !allocate(subs_gl(subtot),subs_gla(subtot))
       !allocate(subs_gl(subtot),subs_gla(subtot),subs_gl_el_min(subtot),subs_gl_el_max(subtot),xx(subtot))
       !subs_gl= 0.
       !subs_gla= 0.
       !subs_gl_el_min=0.
       !subs_gl_el_max=0.
       allocate(gl_elsec_subid(cnt),gl_elsec(cnt),subs_es_gl_a(cnt),&
           subs_es_gl_ra(cnt),subs_es_gl_v(cnt),subs_es_gl_rv(cnt),&
           subs_es_gl_m(cnt),subs_es_gl_wm(cnt),subs_es_bound(cnt))
       subs_es_gl_a=0.
       subs_es_gl_ra=0.
       subs_es_gl_v=0.
       subs_es_gl_rv=0.
       subs_es_gl_m=0.
       subs_es_gl_wm=0.
       subs_es_bound=0.
       do i=1, cnt
      ! read(iu, *, iostat=ierr) id(i), x(i), y(i), z(i)   ! 32	58047.95633	15.467	0.03 & 
       ! read(iu, *, iostat=ierr) id(i), ix(i), x(i), y(i), z(i)   ! 32	15.47	3953	4888	0.03
       read(iu, *, iostat=ierr) id(i), ix(i), x(i), y(i), z(i), a(i),b(i),c(i),d(i)
       gl_elsec_subid(i)=id(i)
       gl_elsec(i)=ix(i)
       subs_es_gl_a(i)=x(i)
       subs_es_gl_ra(i)=y(i)
       subs_es_gl_v(i)=z(i)
       subs_es_gl_rv(i)=a(i)
       subs_es_gl_m(i)=b(i)
       subs_es_gl_wm(i)=c(i)
       subs_es_bound(i)=d(i)

       ! Identify Glacier Subs 
       subs_gl(id(i))=1
       ! Calculate Subbasin Number of ES, Subbasin Glacier Area (km�), Subbasin Total GWE
       if (subs_es_gl_a(i)>0) then
            glsec_num(id(i))=glsec_num(id(i))+1 ! Number of ES per Sub
            gla_sub(id(i))=gla_sub(id(i))+subs_es_gl_a(i) ! Glacier Area per Sub
            glw_sub(id(i))=glw_sub(id(i))+subs_es_gl_m(i) ! Glacier GWE per Sub
            glww_sub(id(i))=glww_sub(id(i))+subs_es_gl_wm(i) ! Glacier Weighted GWE per Sub
       end if
       end do
      
       ! Total Basin Glacier Area according to read in data
       gla_basin=sum(gla_sub)
       glww_basin=sum(glww_sub)
       ! Assign Relative Subbasin Glacier Area & Weighted GWE per Total Glacierized Area & Total Basin Area
       do i=1,idum
            if (subs_gl(i)==1) then
                ! Total Glacier Area or GWE
                gla_fr_sub(i)=gla_sub(i)/gla_basin ! Basin Glacier Area Fraction (Fraction of Subbasin ARea to Total Basin Glacier Area)
                glww_fr_sub(i)=glww_sub(i)/glww_basin ! Basin GWE Fraction (Fraction of Subbasin Weighted GWE to Total Basin GWE)
                ! Basin Area (Subbasin Fraction)
                ! glar_sub2(i)=gla_basin*gla_fr_sub(i) ! Glacier Area per Sub
                glar_sub(i)=gla_sub(i)*sub_fr(i)
                !glwwr_sub2(i)=glww_sub(i)*sub_fr(i)
                !glwwr_sub2(i)=glww_sub(i)*gla_sub(i)/gla_basin
                glwwr_sub2(i)=glww_basin*glww_fr_sub(i) ! Basin Weighted GWE (weighted to total GWE)
                glwwr_sub(i)=glwwr_sub2(i)*sub_fr(i) ! Subbasin Area weighted GWE 
            end if
       end do
        
       ! Number of ES (globally in basin, e.g. 10 as max per subbasin/basin)
       numes=maxval(gl_elsec)
       allocate(escheck(numes)) ! For snom subroutine
       allocate(dh(numes),e_norm(numes))    

       ! Assign Area per ES & Sub
       allocate(es_gl_a(numes,subtot),es_gl_wm(numes,subtot),es_gl_ra(numes,subtot), &
        es_gl_a2(numes,subtot),es_gl_wm2(numes,subtot),es_gl_elev(numes,subtot), &
        es_gl_m2(numes,subtot),es_gl_m(numes,subtot)) ! Allocate ES Subbasin Matrices
       es_gl_a=0.
       es_gl_wm=0.
       es_gl_m=0.
       es_gl_elev=0.
       do i=1,cnt
            !if (i==2) esdist=(subs_es_bound(i)-subs_es_bound(i-1))/2
            if (i>1 .AND. esdist==0) then
                if (subs_es_bound(i-1)>0) esdist=(subs_es_bound(i)-subs_es_bound(i-1))/2
            end if
            if (subs_es_gl_a(i)>0) then
                vol1=vol1+subs_es_gl_a(i)*subs_es_gl_m(i)
                es_gl_a(gl_elsec(i),gl_elsec_subid(i))=subs_es_gl_a(i)
                es_gl_wm(gl_elsec(i),gl_elsec_subid(i))=subs_es_gl_wm(i)
                es_gl_m(gl_elsec(i),gl_elsec_subid(i))=subs_es_gl_m(i)
                es_gl_elev(gl_elsec(i),gl_elsec_subid(i))=subs_es_bound(i)
                es_gl_mx_elev(gl_elsec_subid(i))=subs_es_bound(i)
                if (cnt2==0 .or. gl_elsec_subid(i) /= cnt2) then
                    cnt2=gl_elsec_subid(i)
                    es_gl_mn_elev(gl_elsec_subid(i))=subs_es_bound(i)
                end if         
            end if
       end do    

       !! ===============================================================================================================================
       
       !! d) Create Glacier HRUs
       !! Assign subbasin values to HRUs where needed
       hru_gla=0.
       hru_glww=0.
       hru_glww_gltot=0.
       hru_glww_basin=0.
       hru_glww_sub=0.
       hru_es_id=0.
       hru_fr_es=0.
       count=0
       cnt=0
       es_gl_a2=0.
       es_gl_wm2=0.
       es_gl_m2=0.
       es_gl_ra=0.
       glww_sub2=0.
       test4=0.      
       !! Update Glacier Area of ES & Subbasin based on actual HRU discretization
        do i = 1,subtot ! Loop over Subs 
            do j = 1, hrutot(i) ! Loop over HRUs in that Sub
                cnt=cnt+1
                test4=test4+hru_km(cnt)
                cropname = cpnm(idplt(cnt)) ! LULC Name 
                !! If Land Use of HRU = Glacier
                !! if (idplt(cnt)>=130) then ! Old
                if (index(cropname,"ES")==1) then
                    !! count=idplt(cnt)-129
                    read(cropname(3:),*) count ! Get Index of ES (e.g. ES6 = 6)
                    !! If ES of subbasin i is glacier covered
                    if (es_gl_a(count,i)>0) then
                        hru_gl(cnt)=1 !! Timo GHRU yes or no
                        ! New Parametrization based on HRUs for Glacier Area
                        gla_sub2(i)=gla_sub2(i)+hru_km(cnt) ! Actual Subbasin Glacier Area (based on HRUs)
                        es_gl_a2(count,i) = es_gl_a2(count,i)+hru_km(cnt) ! Actual ES Glacier Area (based on HRUs)
                    else
                        hru_gl(cnt)=0
                    end if
                else
                    hru_gl(cnt)=0
                endif                      
            end do
        end do
        
       ! New Glacier Area on Basin Scale
       gla_basin_hru=sum(es_gl_a2)

       !! Prepare Annual Summary Files for Writing the Outpus
       !!    Glacier Annual Summary Output File
       hedgl = (/" Sub","  ES","  yr","  Asub_km2","   dM_m3","   GWE_mm"," Agl_km2"/)
       !!! Write Heading to Annual Glacier File
       write (89,8999) hedgl
       8999 format (3a4,3a14,1a10)                   
       !8989  format ('TT',i4,1x,i4,1x,i4,f10.3,1x,f10.3,1x,f10.3,1x,f10.3)

       !! Update Glacier Weighted GWE of ES & Subbasin based on actual HRU area
       count=0
       allocate(fractest(size(hru_gl)),fractest2(numes,subtot),es_glmlt(numes,subtot))
       fractest=0
       fractest2=0.
       es_glmlt=0.
       do i = 1,subtot ! Loop over Subs 
           do j = 1, numes ! Loop over ES in that Sub
                !! If ES of Subbasin i is glacier covered
                if (es_gl_a2(j,i)>0) then  
                   ! fac_area=gla_sub(i)/gla_sub2(i) ! As the original WGWE corresponds to the initiliazed area it must be updated with the actual area 
                    !es_gl_wm2(j,i) = glww_sub(i)*es_gl_a2(j,i)/gla_sub2(i)*fac_area ! Actual ES WGWE (based on HRUs and updated Gl. A)
                    fac_area=es_gl_a(j,i)/es_gl_a2(j,i) ! As the original WGWE corresponds to the initiliazed area it must be updated with the actual area 
                    es_gl_wm2(j,i) = es_gl_wm(j,i)*fac_area ! Actual ES WGWE (based on HRUs and updated Gl. A)
                    es_gl_m2(j,i) = es_gl_m(j,i)*fac_area ! Actual ES GWE (based on HRUs and updated Gl. A)
                    glww_sub2(i)=glww_sub2(i)+es_gl_wm2(j,i)  ! Actual Subbasin WGWE (based on HRUs and updated Gl. A)
                    fractest2(j,i)=es_gl_a2(j,i)/gla_sub2(i) ! Fraction of ES of Glacierized Subbasin Area
                    !es_gl_ra(j,i)=es_gl_a2(j,i)/sub_km(i) ! Frac
                    es_gl_ra(j,i)=es_gl_a2(j,i)/gla_sub2(i)
                    vol2=vol2+gla_sub2(i)*es_gl_wm2(j,i)
                end if   
                !! Write Initial GLacier/Sub & ES Specifc Infos to gl_mb_aa.txt 
                !! File contains annual glacier summary of delta-h calculations
                if (gla_sub2(i)>0) then
                    !write (89,*) i, j, iyr, sub_km(i),0,es_gl_wm2(j,i),es_gl_a2(j,i) ! 7 Variables
                    write (89,8998) i,j,iyr-1,sub_km(i),0.000,es_gl_m2(j,i),es_gl_a2(j,i)! 7 Variables
                8998 format (i4,2x,i2,2x,i4,1x,e10.3,1x,e12.4,1x,g12.3,1x,e10.3)                   
                end if
           end do
       gla_fr_sub3(i)=gla_sub2(i)/sub_km(i) ! Glacier Fraction of Subbasin of Total Subbasin Area
       gla_fr_sub2(i)=gla_sub2(i)/gla_basin_hru ! Glacier Fraction of Subbasin of Total Glacier Area
       gla_fr_basin(i)=gla_sub2(i)/test4 ! Glacier Fraction of Subbasin of Total Basin Area
       end do
       
       !! Volume Difference of Initialization 
       !! Diff between original Vol (water equivalent) & HRU based SWAT Volume
       error=(vol2-vol1)/vol1
       
       !! Initialize Glacier HRUS (HRU Areas & GWEs based on different Scales such as Basin, Total Gl. A, Subbasin etc.)
       count=0
       cnt=0
       do i = 1,subtot ! Loop over Subs 
           do j = 1, hrutot(i) ! Loop over HRUs in that Sub
               cnt=cnt+1
               cropname = cpnm(idplt(cnt)) ! LULC Name
               !! If Land Use of HRU = Glacier
               if (index(cropname,"ES")==1) then 
                    read(cropname(3:),*) count ! Get Index of ES (e.g. ES6 = 6)
                    !! If ES of subbasin i is glacier covered
                    if (es_gl_a2(count,i)>0) then
                        test2=hru_km(cnt)/es_gl_a2(count,i) !! HRU Fraction of ES
                        test3=hru_km(cnt)/gla_basin_hru  !! HRU Fraction of Total Glacier Area (Basin Glacierized)
                        test5=hru_km(cnt)/gla_sub2(i)  !! HRU Fraction of Total Glacierized Subbasin Area
                        
                        ! HRU GWE on ES Scale
                        hru_gla(cnt)=es_gl_a2(count,i)*test2 !! Timo Glacier Area of GHRU based on HRU Fraction of ES
                        
                        ! Option 1 (weighted ES for glacier)
                        !hru_glww(cnt)=es_gl_wm2(count,i)*test2 !! Timo Weighted GWE of GHRU based on HRU Fraction of ES
                        !hru_glww2(cnt)=es_gl_wm2(count,i) !! All HRUs of ES get same GWE of that ES 
                        
                        ! Option 2 (unweighted ES for glacier)
                        hru_glww(cnt)=es_gl_m2(count,i)*test2 !! Timo Weighted GWE of GHRU based on HRU Fraction of ES
                        hru_glww2(cnt)=es_gl_m2(count,i) !! All HRUs of ES get same GWE of that ES 
                        
                        fractest(cnt)=es_gl_a2(count,i)/gla_sub2(i) ! ES Fraction of Total Glacierized Subbasin Area
                        hru_es_id(cnt)= count ! Assign ES (lower or upper part check) to HRU
                        hru_fr_es(cnt)=test2 ! HRU fraction of ES
                        
                        !fractest(cnt)=hru_km(cnt)/es_gl_a2(count,i)
                        
                        ! HRU GWE on Total Glacier Area Scale
                        !hru_gla(cnt)=gla_basin_hru*test3 !! Timo Glacier Area of GHRU based on HRU Fraction of ES
                        !hru_glww(cnt)=es_gl_wm2(count,i)/fractest(cnt)*test3 !! Timo Weighted GWE of GHRU based on HRU Fraction of ES
                        hru_glww_gltot(cnt)=glww_sub2(i)*test3 !! Timo Weighted GWE of GHRU based on Total Gl. Area

                        ! HRU GWE on Basin Scale
                        hru_glww_basin(cnt)=glww_sub2(i)*hru_km(cnt)/test4 !! Timo Weighted GWE of GHRU based on Total Basin Area 
                        
                        ! HRU GWE on Subbasin Scale
                        hru_glww_sub(cnt)=glww_sub2(i)*hru_km(cnt)/sub_km(i)
                        
                        ! HRU Glacier Height                      
                        hru_gl_elev(cnt)=es_gl_elev(count,i)
                        !vol3=vol3+(hru_glww(cnt)*gla_sub2(i))
                        !vol3=vol3+(hru_glww2(cnt)*gla_sub2(i)*test2)
                        vol3=vol3+(hru_glww2(cnt)*es_gl_a2(count,i)*test2)
                        
                    else
                       hru_gla(cnt)=0   
                       hru_glww(cnt)=0
                       hru_glww_basin(cnt)=0
                       hru_glww_gltot(cnt)=0
                       hru_gl_elev(cnt)=0
                       hru_glww2(cnt)=0
                       hru_glww_sub(cnt)=0
                    end if
               !! If LULC no glacier
               else
                   hru_gla(cnt)=0
                   hru_glww(cnt)=0
                   hru_glww_basin(cnt)=0
                   hru_glww_gltot(cnt)=0
                   hru_gl_elev(cnt)=0
                   hru_glww2(cnt)=0
                   hru_glww_sub(cnt)=0
               endif                      
           end do
       end do
       end if
       
       ! Format of gl_mb_aa.txt file which is initialized here
      ! 1000 format ('TT',i4,1x,i4,1x,i4,f10.3,1x,f10.3,1x,f10.3,1x,f10.3)

       
       !! Control Calculations (for mass balance check) 
       ! Updated Basin Glacier Area & GWE
       ! due to HRU Creation where holes are filled with the remaining HRU sharings 
       ! it might be that the HRU Glacier Area might slightly differ from the originally read in HRU area from the ES 
       !gla_basin_hru=sum(hru_gla)
       !glww_basin_hru=sum(hru_glww)              
       !error1=100*((gla_basin_hru-gla_basin)/gla_basin) ! Areal Difference between Original and SWAT Internal Calculated Area
       !error2=100*((glww_basin_hru-glww_basin)/glww_basin) ! GWE Difference between Original and SWAT Internal Calculated Area
       ! 
       !error1=0
       !error2=0
       !! Check the Volume of the different GWE calculations and the corresponding reference areas
       !do i=1,size(hru_gl)
       !     error1=error1+hru_glww_gltot(i)*gla_basin_hru
       !     error2=error2+hru_glww_basin(i)*test4
       !     !error2=error2+hru_glww(i)*hru_gla(i)
       !end do
       

    return   
    end        
