!! Timo Subroutine
!! Reads in Glacier Infos for ES & EB of each Subbasin
!! Here the Initialization of Glacier Areas takes place
       subroutine readgles_t
            
       use parm, only:subtot, hrutot, hru_sub, subs_gl, subs_gla, hru_gl, hru_gla, &
           subs_gl_elsec, subs_gl_mass, subs_gl_el_min, subs_gl_el_max, glsec_num, &
           gl_elsec, gl_elsec_subid, subs_eb_gl_a, subs_es_gl_a, sub_es_cum_gl_a, &
           subs_es_bound
       integer:: IU, ierr, cnt, I, cnt2
       integer:: idum,iglhru,div,divall
       real:: tmp1, tmp2, tmp3
       integer, dimension(:), allocatable:: ID, es
       integer, dimension(:,:), allocatable:: eb
       !integer, dimension(:,:), allocatable:: ebtest
       double precision, dimension(:), allocatable:: x,y,z,ix
       integer:: interval ! Elevation Section Spacing of Glaciers
       integer:: skip
       skip=0
       if (skip==1) THEN

       !real:: eb_bounds
       !eb_bound =[2300,2700,3100,3650,max([subs(:).ElevMax])]
       !! --------------------------------------------------------------------------------
       !! a) Read in Glacier Elevation Band Information
       ! open(newunit=IU, file='swat_sub_glaciers.txt',status='old',action='read') ! Variante 1
       open(newunit=IU, file='swat_gn_test_gleb.txt',status='old',action='read') ! Variante 1
       read(iu,'(A)') titldum
       ierr = 0
       cnt  = 0    
       do while(ierr == 0)
           read(iu, *, iostat=ierr) idum, tmp1   ! Sub/Gl Elev Band  Rel Area
           cnt = cnt + 1
       end do
       cnt = cnt - 1
       if(cnt > 0) THEN
           allocate(id(cnt),y(cnt))    
       else 
           error stop 'empty file'
       end if
       rewind(iu)
       read(iu,'(A)') titldum
       ierr = 0
       allocate(subs_eb_gl_a(10,subtot),eb(10,subtot))
       !allocate(ebtest(10,43))
       !ebtest = 0.
       subs_eb_gl_a= 0.
       eb=0.
       ! Read Rel Gl Share within all Subs and EB to y
       do i=1, cnt
          read(iu, *, iostat=ierr) id(i), y(i)  ! Sub/Gl Elev Section  Rel Area	0.03
       end do
       cnt2=0
       ! Transfer rel. Glacier Area to EB Subbasin Matrix
       do i=1,subtot
           do ii=1,10
               cnt2=cnt2+1
               subs_eb_gl_a(ii,i)=y(cnt2)
           enddo
       enddo
       
       !! -------------------------------------------------------------------------------------
       !! b) Read in Glacier Elevation Section (ES) Infos
       deallocate(y,id)
       open(newunit=IU, file='swat_gles_full.txt',status='old',action='read') ! Variante 1
       read(iu,'(A)') titldum ! Read Header
       ierr = 0
       cnt  = 0
       ! Read Actual Data 
       do while(ierr == 0)
           read(iu, *, iostat=ierr) idum, tmp1, tmp2   ! Sub/Gl Elev Section  Rel Area of total Glacier Area
           cnt = cnt + 1
       end do
       cnt = cnt - 1
       if(cnt > 0) THEN
           allocate(id(cnt),y(cnt),z(cnt))    
       else 
           error stop 'empty file'
       end if
       rewind(iu)
       read(iu,'(A)') titldum
       ierr = 0
       allocate(subs_es_gl_a(cnt),es(cnt),sub_es_cum_gl_a(cnt),subs_es_bound(cnt))
       subs_es_gl_a= 0.
       sub_es_cum_gl_a = 0.
       subs_es_bound = 0.
       es=0.
       ! Read Rel Gl Share within all Subs and EB to y
       do i=1, cnt
          read(iu, *, iostat=ierr) id(i), y(i), z(i)  ! Sub/Gl Elev Section  Rel Area	0.03
       end do
       !! Assign Text File Data & Cumsum of Glacier ES per Subbasin to Array
       do i=1,cnt ! Loop over total number of ES (all sub)
           subs_es_gl_a(i)=y(i) ! Assign Rel. Gl Area of ES
           subs_es_bound(i)=z(i) ! Assign Upper ES Bound
           if (i==1) then ! If 1. ES oder Neue Nummerierung bei Neuem Sub oder Zwischendrin
               sub_es_cum_gl_a(i)=y(i) ! Assign Cumsum of ES in certain Sub
           elseif (id(i) .NE. id(i-1)) then
               sub_es_cum_gl_a(i)=y(i) ! Assign Cumsum of ES in certain Sub
               ! Correct rounding errors if cumsum of Gl ES are > 1
               if (sub_es_cum_gl_a(i-1)>1) then
                   sub_es_cum_gl_a(i-1)=1 ! Assign Cumsum of ES in certain Sub
               endif
           else 
               sub_es_cum_gl_a(i) = sub_es_cum_gl_a(i-1) + y(i) ! Assign Cumsum of ES in certain Sub
           endif
           ! Enumeration of ES (all subs)
           es(i) = id(i)
       end do

       end if
       return  
       end