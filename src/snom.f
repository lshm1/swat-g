      subroutine snom
      
!!    ~ ~ ~ PURPOSE ~ ~ ~
!!    this subroutine predicts daily snom melt when the average air
!!    temperature exceeds 0 degrees Celcius

!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    elevb(:,:)   |m             |elevation at center of band
!!    elevb_fr(:,:)|none          |fraction of subbasin area within elevation 
!!                                |band
!!    iida         |julian date   |day being simulated (current julian date)
!!    ihru         |none          |HRU number
!!    pcpband(:,:) |mm H2O        |precipitation for the day in band in HRU
!!    precipday    |mm H2O        |precipitation on the current day in the HRU
!!    sub_sftmp    |deg C         |Snowfall temperature
!!                                |Mean air temperature at which precipitation
!!                                |is equally likely to be rain as snow/freezing
!!                                |rain.
!!    sub_smfmn    |mm/deg C/day  |Minimum melt rate for snow during year (Dec.
!!                                |21) where deg C refers to the air temperature
!!    sub_smfmx    |mm/deg C/day  |Maximum melt rate for snow during year (June
!!                                |21) where deg C refers to the air temperature
!!                                |SMFMX and SMFMN allow the rate of snow melt
!!                                |to vary through the year. These parameters
!!                                |are accounting for the impact of soil
!!                                |temperature on snow melt.
!!    sub_smtmp    |deg C         |Snow melt base temperature
!!                                |Mean air temperature at which snow melt will 
!!                                |occur.
!!    sno_hru(:)   |mm H2O        |amount of water in snow in HRU on current day
!!    snocov1      |none          |1st shape parameter for snow cover equation
!!                                |This parameter is determined by solving the
!!                                |equation for 50% snow cover
!!    snocov2      |none          |2nd shape parameter for snow cover equation
!!                                |This parameter is determined by solving the
!!                                |equation for 95% snow cover
!!    snocovmx     |mm H2O        |Minimum snow water content that corresponds
!!                                |to 100% snow cover. If the snow water content
!!                                |is less than SNOCOVMX, then a certain 
!!                                |percentage of the ground will be bare.
!!    snoeb(:,:)   |mm H2O        |snow water content in elevation band on 
!!                                |current day
!!    snotmp(:)    |deg C         |temperature of snow pack in HRU
!!    snotmpeb(:,:)|deg C         |temperature of snow pack in elevation band
!!    tavband(:,:) |deg C         |average temperature for the day in band in HRU
!!    sub_timp     |none          |Snow pack temperature lag factor (0-1)
!!                                |1 = no lag (snow pack temp=current day air
!!                                |temp) as the lag factor goes to zero, the
!!                                |snow pack's temperature will be less
!!                                |influenced by the current day's air 
!!                                |temperature
!!    tmpav(:)     |deg C         |average air temperature on current day for 
!!                                |HRU
!!    tmx(:)       |deg C         |maximum air temperature on current day for 
!!                                |HRU
!!    tmxband(:,:) |deg C         |maximum temperature for the day in band in HRU
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name         |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    precipday    |mm H2O        |amount of water in effective precipitation
!!                                |in HRU
!!    precipdt(:)  |mm H2O        |precipitation for the time step during day
!!    sno_hru(:)   |mm H2O        |amount of water in snow in HRU on current day
!!    snoeb(:,:)   |mm H2O        |snow water content in elevation band on 
!!                                |current day
!!    snofall      |mm H2O        |amount of precipitation falling as freezing 
!!                                |rain/snow on day in HRU
!!    snomlt       |mm H2O        |amount of water in snow melt for the day in 
!!                                |HRU
!!    snotmp(:)    |deg C         |temperature of snow pack in HRU
!!    snotmpeb(:,:)|deg C         |temperature of snow pack in elevation band
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
!!    ib          |none          |counter
!!    j           |none          |HRU number
!!    smfac       |
!!    smleb       |mm H2O        |amount of snow melt in elevation band on 
!!                               |current day
!!    smp         |mm H2O        |precipitation on current day for HRU
!!    snocov      |none          |fraction of HRU area covered with snow
!!    sum         |mm H2O        |snow water content in HRU on current day
!!    xx          |none          |ratio of amount of current day's snow water
!!                               |content to the minimum amount needed to
!!                               |cover ground completely
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: Real, Sin, Exp

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~


      use parm

      integer :: j, ib, k
      real :: sum, glsum, smp, smfac,smfac2, smleb, gmlteb, b, glacc_eb
      real :: xx, snocov, snomlt2, xx2, smleb2, diff, glmltfac, res, cum
      real :: gmltsum,smltsum,glaccsum, error
      !real, dimension (:,:), allocatable :: gleb
      ! gl_exp=0. !! Timo Glacier Exposed Area
      ! gl_sc=0. !! Timo Glacier Snow Cover
   !   gleb=0. !! Timo Glacier Storage
      gmlteb=0. !! Timo Glacier Melt in EB
      glacc_eb=0. !! Timo Glacier Accumulation in EB
      !frefr=0.2 !! Timo Refreezing Share of Glacier Melt
      !escheck=0.
      k =0
      error=0
      j = 0
      j = ihru
      sum =0.
      smp =0.
      pt=0. ! New SMLT Module yes or no
      pthr=1 ! Threshold pcp in mm
      glsum = 0. !! Timo Glacier Intermediate Calculation Step
      isub = hru_sub(j)
      snocovavg2 = 0
      dwmlt=1
      
      !! --------------------------------------------------------------------------------------------------------
      !! Elevation Bands
      if (elevb(1,isub) > 0. .and. elevb_fr(1,isub) > 0.) then ! If EB active

        !! compute snow fall and melt for each elevation band
        do ib = 1, 10      
          if (elevb_fr(ib,isub) < 0.) exit
          snotmpeb(ib,j) = snotmpeb(ib,j) * (1.-sub_timp(ib,isub)) +
     &                                tavband(ib,j) * sub_timp(ib,isub)

          if (tavband(ib,j) < sub_sftmp(ib,isub)) then
            !! compute snow fall if temperature is below sftmp
            snoeb(ib,j) = snoeb(ib,j) + pcpband(ib,j)
            snofall = snofall + pcpband(ib,j) * elevb_fr(ib,isub)
          end if
          !! Timo Snow Redistribution between elevation bands
          if (ib<10) then
              if (snoeb(ib+1,j) > snoeb(ib,j) .AND. 
     &           snoeb(ib+1,j) >2000) then
                 t_redist=snoeb(ib+1,j)-2000 !Duethmann uses only glacier free areas: (1-glcov)*snoeb(ib+1,j)-3000
                 snoeb(ib+1,j)=snoeb(ib+1,j)-t_redist 
                 snoeb(ib,j)=snoeb(ib,j)+t_redist
              endif
          endif
          !! Snow Cover Calculations
          if (snoeb(ib,j) < snocovmx) then
            xx = 0.
            snocov = 0.
            xx = snoeb(ib,j) / snocovmx
            snocov = xx / (xx + Exp(snocov1 - snocov2 * xx))   
          else
            snocov = 1.                
          endif
          snocovavg2 = snocovavg2 + snocov * elevb_fr(ib,isub) !! Timo (HRU Snocov of all EB->sums up the individual snocovs)
          snocovavg(isub) = snocovavg2 !! Timo
          snocov22(ib,j) = snocov !! Timo 
          
          !! compute snow melt if temperature is above smtmp
            if (tmxband(ib,j) > sub_smtmp(ib,isub)) then
                smfac = 0.
                smleb = 0.
                !! Original bmelt
                smfac = (sub_smfmx(ib,isub) + sub_smfmn(ib,isub)) / 2. +  
     &            Sin((iida - 81) / 58.09) *                              
     &            (sub_smfmx(ib,isub) - sub_smfmn(ib,isub)) / 2.    !! 365/2pi = 58.09
                !! Adjust for wet days where liquid precip contributes to additional melt (POS)
                if (pt==1) then
                  if (tavband(ib,j) > sub_sftmp(ib,isub) .and. 
     &               pcpband(ib,j) > pthr) then 
                    smfac = smfac + dwmlt*(pcpband(ib,j)-pthr)
                  endif
                    
                elseif (pt==2) then !! Hock 1999 HTI 
                      smfac_hti = 1 !! Melt Factor [mm /d C�]
                      radfac = 0.2 !! Radiation Coefficient [mm m�/d W �C]
                      Ipot = hru_rmx(j)/(3600*24/10**6) !! Potential Direct Solar Radiation [W/m�] -> Conversion from MJ/m^2
                      smleb = (smfac_n + radfac*Ipot)*(((snotmpeb(ib,j)+ 
     &                     tmxband(ib,j)) / 2.) - sub_smtmp(ib,isub)) 
                elseif (pt==3) then !! Enhanced TI (ETI) Pellicciotti et al. 2017
                      smfac_eti = 1 !! Melt Factor [mm /d C�]
                      radfac = 0.2 !! SWR Factor [mm m�/d W]
                      alpha=albday !! Albedo
                      G = hru_ra(j)/(3600*24/10**6) !! HRU SWR [W/m�] -> Conversion from MJ/m^2
                      smleb = (smfac_n *(((snotmpeb(ib,j) + 
     &                     tmxband(ib,j)) / 2.) - sub_smtmp(ib,isub))) +
     &                     (radfac*(1-alpha) * G)                                                
                endif
                smleb = smfac * (((snotmpeb(ib,j) + tmxband(ib,j)) / 2.)  
     &                                             - sub_smtmp(ib,isub))              
                
                !! adjust for areal extent of snow cover
                smleb = smleb * snocov               
                if (smleb < 0.) smleb = 0.
                if (smleb > snoeb(ib,j)) smleb = snoeb(ib,j)
                snoeb(ib,j) = snoeb(ib,j) - smleb
                snomlt = snomlt + smleb * elevb_fr(ib,isub)                               
            end if
            
            !! Timo Glacier Area / Snow Covered Area
            if (snocov >= gla_fr_sub3(isub)) then ! Snowcover > Glacier Cover of Subbasin
                !gl_sc(j) = 1 !! Snow covered Glacier Area of HRU (= Fully Snow Covered)
                gl_exp(j) = 0 !! Exposed Glacier Area of HRU (= No Exposed Area)                
                gl_sc(j) = snocov  !! Snow covered Glacier Area of HRU (= Fully Snow Covered)
            elseif (snocov < gla_fr_sub3(isub)) then
                gl_exp(j) = gla_fr_sub3(isub) - snocov  !! Exposed Glacier Area of HRU 
                gl_sc(j) = snocov  !! Snow Covered Area of HRU (% Share of)
            endif           
                       
            !! Accumulation (Snow Metamorphosis)
            if (hru_gl(j)==1) then
                iiacc=0 ! Accumulation Method
                !! Wortmann Accumulation
                if (iiacc==2) then
                ! *to be added* 
          
                !! Seibert Accumulation
                elseif (iiacc==1) then
                ! f_accu based on simple constant transformation value (between 0.001 and 0.003)
                glacc_eb = sno_hru(j) * (1-f_accu(j)) !! Timo Glacier Accumulation of HRU

                !! Luo Accumulation
                elseif (iiacc==0) then
                !! f_accu = 0.003 [0.001 and 0.003]          
                if (snoeb(ib,j)>0) then
                    b = f_accu(j) * (1 + sin(0.0172 * (iida - 81)))
                    glacc_eb = snoeb(ib,j) * b !! Timo Glacier Accumulation of HRU
                endif
                glacc_eb = glacc_eb * gl_sc(j) !! Analogously to glmlt -> adapt to snow covered area
                endif
              
                !! Correct SWE for glacier accumulation
                if (glacc_eb < 0.) glacc_eb = 0.
                if (glacc_eb > snoeb(ib,j)) glacc_eb = snoeb(ib,j)
                snoeb(ib,j) = snoeb(ib,j) - glacc_eb
                glacc = glacc + glacc_eb * elevb_fr(ib,isub) 
            end if
            
          !! TIMO Glacier Melt
          if (tmxband(ib,j) > glmtmp(j) .and. hru_gl(j)==1 .and. 
     &                  (snoeb(ib,j) < snocovmx)) then ! If T > Tglmlt & HRU = Glacier HRU & snocov ~= 1
              glmltfac=0
              gmlteb=0
              glmltfac = (glmfmx(j) + glmfmn(j)) / 2.+
     &          Sin((iida - 81) / 58.09) *                              
     &          (glmfmx(j)  - glmfmn(j) ) / 2.    !! 365/2pi = 58.09
              !! Correct Gl Melt Factor if < Snow Melt Factor
              if (smfac>glmltfac) then
                  gltmltfac=smfac
              end if
              gmlteb = glmltfac * (tmxband(ib,j)-glmtmp(j))
              gmlteb=(1-f_frze(j))*gmlteb ! Refreezing -> Reduction of glmlt                                     
              
              !! Timo Glacier Area / Snow Covered Area
              if (snocov >= gla_fr_sub3(isub)) then ! Snowcover > Glacier Cover of Subbasin
                  gl_exp(j) = 0 !! Exposed Glacier Area of HRU (= No Exposed Area)                
                  gl_sc(j) = snocov  !! Snow covered Glacier Area of HRU (= Fully Snow Covered)
                  gmlteb=0 !! No melt takes place then
              elseif (snocov < gla_fr_sub3(isub)) then
                  gl_exp(j) = gla_fr_sub3(isub) - snocov  !! Snow covered Glacier Area of HRU                   
                  gl_sc(j) = snocov  !! Exposed Glacier Area of HRU (% Share of)
                  gmlteb = gmlteb * gl_exp(j) !! Actual Melt for EB
              endif   
                            
              ! TIMO Glacier Melt
              glmlt = glmlt + gmlteb * elevb_fr(ib,isub) !! TIMO Glacier Melt of HRU 

          endif
          sum = sum + snoeb(ib,j) * elevb_fr(ib,isub) !! HRU Sum of SWE (HRU unweighted/EB weighted)
          smp = smp + pcpband(ib,j) * elevb_fr(ib,isub) !! HRU Sum of Pcp (HRU unweighted/EB weighted)
      end do
      
        !! Glacier Melt & Accumulation Correction if Glacier is not fully snow covered (gl_exp > 0, melt can occur)
        !! 2 Cases: (1) ES fully exposed -> full glacier melt can occur, (2) ES partly snow covered -> fractional melt occurs
        if (gl_exp(j)>0) then
        xx2=0.
        cum = 0.
        res=0.
        !hru_es_id=0.
        escheck=0.
        do ja = 1, numes ! Loop over ES in that Sub
            ! Check which ES are snow covered
            ! Assumption: if Aes,ia > Aexp,j then this first (lowest elevated) ES will be melted
            ! If Aexp > Aes then the lowest ES & the second lowest will receive melt as well and so on.
            ! This is done until the exposed area is distributed over the ES. 
            if (es_gl_a2(ja,isub)>0) then ! If Glacier ES 
                cum=cum+es_gl_ra(ja,isub) ! Cumulative ES subbasin fraction
                xx2 = gl_exp(j)-cum  ! Residual Area between exposed gl. area and cumulative ES Area                              
                ! If Residual Area > 0 (Exposed Gl. Area > ES Area) -> ES is fully exposed
                if (xx2>=0) then
                    escheck(ja)=1  
                    !res=res+xx2
                    ! If current HRU belongs to ES which is exposed 
                    if (hru_es_id(j)==ja) then
                        glmlt=glmlt ! Glacier Melt remains as it is
                        !glacc=0
                    endif
                else ! If Residual < 0
                    escheck(ja)=(es_gl_ra(ja,isub)+xx2)/
     &                         es_gl_ra(ja,isub)! Calculation of Areal correction factor so that glacier melt is only partly considered (as xx2 is negative we take the sum)
                    ! If current HRU belongs to ES which is exposed 
                    if (hru_es_id(j)==ja) then
                        ! Check if ES is the first which is only partly exposed otherwise no melt
                        if (ja>1)then ! If not ES=1 but a higher elevated one with already exposed ES before
                        if (escheck(ja-1)==1) then ! If previous ES was fully exposed -> everything ok and melt is corrected (Following ES not exposed anymore)
                            glmlt=glmlt*escheck(ja) ! Glacier Melt is corrected
                            !glacc=glacc*xx2
                        elseif (escheck(ja-1)==0) then ! If previous ES was not exposed at all -> everything ok and melt is corrected (Following ES not exposed anymore)
                            glmlt=glmlt*escheck(ja) ! Glacier Melt is corrected
                            !glacc=glacc*xx2
                        else ! If previous ES was already partly exposed this one and following ES are not exposed anymore
                            glmlt=0
                            escheck(ja)=0
                        endif
                        else ! If ES=1 and only partly exposed
                            glmlt=glmlt*escheck(ja) ! Glacier Melt is corrected
                            !glacc=glacc*xx2
                        endif                                   
                    endif
                endif                                         
            endif
        end do                 
        endif
        
        !! Glacier Water Storage Change
        !! Glacier Accum Correction
        if (glacc < 0.) glacc = 0.
        if (glmlt < 0.) glmlt = 0.
        if (glmlt > hru_glww2(j)) glmlt = hru_glww2(j)        
        !if (glmlt*hru_fr_es(j) > hru_glww(j)) glmlt = hru_glww(j)
        hru_glww(j) = hru_glww(j) + ((glacc - glmlt)*hru_fr_es(j)) !! Timo Glacier Storage Change HRU Specific per ES
        hru_glww2(j) = hru_glww2(j) + glacc - glmlt !! Timo Glacier Storage Change Uniform per ES
        if (hru_glww(j)<0) hru_glww(j)=0

        !! Timo Modification Snow Cover
        if (snocovavg2 < 0.0) then
              snocovavg(isub)=0
        endif
        !! Add/Sub aggregate snow fall and melt from effective precip 
        !! and snow cover
        precipday = smp + snomlt - snofall + glmlt
        if (precipday < 0.) precipday = 0.
        if (nstep > 0) then
          do ii = 1, nstep
            precipdt(ii+1) = precipdt(ii+1) + (snomlt + glmlt - snofall)
     &                      / nstep
            if (precipdt(ii+1) < 0.) precipdt(ii+1) = 0.
          end do
        end if
        sno_hru(j) = sum !! HRU SWE Summed over Elevation Bands 
        snocov23(j) = snocovavg2 !! Timo Snow Covver

      else
    !! ====================================================================================================================================================
    !! No elevation bands      
	ib = 1
        !! Estimate snow pack temperature
        snotmp(j)=snotmp(j) * (1. - sub_timp(ib,isub)) + tmpav(j) *     
     &            sub_timp(ib,isub)
        !! Snowfall
        if (tmpav(j) <= sub_sftmp(ib,isub)) then
          !! calculate snow fall
          sno_hru(j) = sno_hru(j) + precipday
          snofall = precipday
          precipday = 0.
          precipdt = 0.
        endif
        !! Snow Cover
        if (sno_hru(j) < snocovmx) then
            xx = 0.
            xx = sno_hru(j) / snocovmx
            snocov = xx / (xx + Exp(snocov1 - snocov2 * xx))
        else
            snocov = 1.
        endif
        snocov21(j) = snocov !! Timo
        snocovavg2 = snocov   
        !! Snow Melt Calculations
        if (tmx(j) > sub_smtmp(ib,isub) .and. sno_hru(j) > 0.) then ! If Temp > SMTMP & SWE > 0 
          !! adjust melt factor for time of year
          smfac = 0.
          snomlt = 0.
          smfac = (sub_smfmx(ib,isub) + sub_smfmn(ib,isub)) / 2. +      
     &       Sin((iida - 81) / 58.09) *                                 
     &       (sub_smfmx(ib,isub) - sub_smfmn(ib,isub)) / 2.  !! 365/2pi = 58.09
          !! adjust for wet days where liquid precip contributes to additional melt
          if (pt==1) then
          if (tmpav(j) > sub_sftmp(ib,isub) .and. 
     &               precipday > pthr) then 
                    smfac = smfac + dwmlt*(pcpband(ib,j)-pthr)          
          endif
          endif
          snomlt = smfac * (((snotmp(j)+tmx(j))/2.)-sub_smtmp(ib,isub)) !! Standard Snowmelt
          
          !! adjust for areal extent of snow cover          
          snomlt = snomlt * snocov   
          if (snomlt < 0.) snomlt = 0.
          if (snomlt > sno_hru(j)) snomlt = sno_hru(j)
          sno_hru(j) = sno_hru(j) - snomlt
        else
          snomlt = 0. ! If T<Tmelt
        end if

        !! Timo Glacier Area / Snow Covered Area
        if (snocov >= gla_fr_sub3(isub)) then ! Snowcover > Glacier Cover of Subbasin
            gl_sc(j) = 1 !! Snow covered Glacier Area of HRU (= Fully Snow Covered)
            gl_exp(j) = 0 !! Exposed Glacier Area of HRU (= No Exposed Area)
        elseif (snocov < gla_fr_sub3(isub)) then
            gl_exp(j) = gla_fr_sub3(isub) - snocov  !! Snow covered Glacier Area of HRU 
            gl_sc(j) = 1 - gl_exp(j)  !! Exposed Glacier Area of HRU (% Share of)
        endif
        
        !! Accumulation (Snow Metamorphosis)
        if (hru_gl(j)==1) then
            !! Wortmann Accumulation
            iiacc=0 ! Accumulation Method
            if (iiacc==2) then
            ! *to be added* 
          
            !! Seibert Accumulation
            elseif (iiacc==1) then
            ! f_accu based on simple constant transformation value (between 0.001 and 0.003)
            glacc = sno_hru(j) * (1-f_accu(j)) !! Timo Glacier Accumulation of HRU

            !! Luo Accumulation
            elseif (iiacc==0) then
            !! f_accu = 0.003 [0.001 and 0.003]          
            if (sno_hru(j)>0) then
                b = f_accu(j) * (1 + sin(0.0172 * (iida - 81)))
                glacc = sno_hru(j) * b !! Timo Glacier Accumulation of HRU
            endif
            glacc = glacc * gl_sc(j) !! Analogously to glmlt -> adapt to snow covered area
            end if
        
            ! Correct SWE for glacier accumulation
            if (glacc < 0.) glacc = 0.
            if (glacc > sno_hru(j)) glacc = sno_hru(j)
            sno_hru(j) = sno_hru(j) - glacc
        end if
        
          !! Timo Glacier Melt    
          ! sub_glmlttmp(ib,isub)=sub_smtmp(ib,isub)
          if (tmx(j) > glmtmp(j) .and. hru_gl(j)==1) then ! If T > Tglmlt & HRU = Glacier HRU          
              glmltfac = (glmfmx(j) + glmfmn(j)) / 2.+
     &          Sin((iida - 81) / 58.09) *                              
     &          (glmfmx(j) - glmfmn(j)) / 2.    !! 365/2pi = 58.09        
              !! Correct Gl Melt Factor if < Snow Melt Factor
              if (smfac>glmltfac) then
                  gltmltfac=smfac
              end if
              glmlt = glmltfac * (tmx(j) - glmtmp(j)) ! Glacier Melt
              glmlt = (1-f_frze(j))*glmlt ! Refreezing -> Reduction of Glacier Melt
              
              !! Timo Glacier Area / Snow Covered Area
              if (snocov >= gla_fr_sub3(isub)) then ! Snowcover > Glacier Cover of Subbasin
                  gl_exp(j) = 0 !! Exposed Glacier Area of HRU (= No Exposed Area)                
                  gl_sc(j) = snocov  !! Snow covered Glacier Area of HRU (= Fully Snow Covered)
                  glmlt=0 !! No melt takes place then
              elseif (snocov < gla_fr_sub3(isub)) then
                  gl_exp(j) = gla_fr_sub3(isub) - snocov  !! Snow covered Glacier Area of HRU                   
                  gl_sc(j) = snocov  !! Exposed Glacier Area of HRU (% Share of)
                  glmlt = glmlt * gl_exp(j)
              endif  
            end if  
            !! Glacier Melt & Accumulation Correction if Glacier is not fully snow covered (gl_exp > 0, melt can occur)
            ! 2 Cases: (1) ES fully exposed -> full glacier melt can occur, (2) ES partly snow covered -> fractional melt occurs
            if (gl_exp(j)>0) then  
            xx2=0.
            cum = 0.
            res=0.
            !hru_es_id=0.
            escheck=0.
            do ja = 1, numes ! Loop over ES in that Sub
                    ! Check which ES are snow covered
                    ! Assumption: if Aes,ia > Aexp,j then this first (lowest elevated) ES will be melted
                    ! If Aexp > Aes then the lowest ES & the second lowest will receive melt as well and so on.
                    ! This is done until the exposed area is distributed over the ES. 
                    if (es_gl_a2(ja,isub)>0) then ! If Glacier ES 
                        cum=cum+es_gl_ra(ja,isub) ! Cumulative ES subbasin fraction
                        xx2 = gl_exp(j)-cum  ! Residual Area between exposed gl. area and cumulative ES Area                              
                        ! If Residual Area > 0 (Exposed Gl. Area > ES Area) -> ES is exposed
                        if (xx2>=0) then
                            escheck(ja)=1  
                            !res=res+xx2
                            ! If current HRU belongs to ES which is exposed 
                            if (hru_es_id(j)==ja) then
                                glmlt=glmlt ! Glacier Melt remains as it is
                                !glacc=0
                            endif
                        else ! If Residual < 0
                            escheck(ja)=(es_gl_ra(ja,isub)+xx2)/
     &                                    es_gl_ra(ja,isub) ! Calculation of Areal correction factor so that glacier melt is only partly considered (as xx2 is negative we take the sum)
                            ! If current HRU belongs to  ES which is exposed 
                            if (hru_es_id(j)==ja) then
                                ! Check if ES is the first which is only partly exposed otherwise no melt
                                if (ja>1) then ! If not ES=1 but a higher elevated one with already exposed ES before
                                if (escheck(ja-1)==1) then ! If previous ES was fully exposed -> everything ok and melt is corrected (Following ES not exposed anymore)
                                    glmlt=glmlt*escheck(ja) ! Glacier Melt is corrected
                                    !glacc=glacc*xx2
                                elseif (escheck(ja-1)==0) then ! If previous ES was not exposed at all -> everything ok and melt is corrected (Following ES not exposed anymore)
                                    glmlt=glmlt*escheck(ja) ! Glacier Melt is corrected
                                    !glacc=glacc*xx2
                                else ! If previous ES was already partly exposed this one and following ES are not exposed anymore
                                    glmlt=0
                                    escheck(ja)=0
                                endif
                                else ! If ES=1 and only partly exposed
                                    glmlt=glmlt*escheck(ja) ! Glacier Melt is corrected
                                    !glacc=glacc*xx2
                                endif                                   
                            endif
                        endif                                         
                    endif
                end do                 
            endif
                                                                  
          !! Glacier Melt Correction
          ! Note: For Uniform distributed GWEs across HRUs (hru_glww2) of an ES, the melt can be directly removed equally from all HRUs
          ! No further fraction calculation is then necessary (the individual HRUs are then not allowed to be multiplied all with the ES Area, 
          ! but only one of the similar values of all HRUs of an ES)
          ! If the ES fraction weighted HRU GWEs (hru_glww) are taken the melt must be multiplied with the HRU ES fraction,
          ! and the individual HRUs of an ES must be then summed and multiplied with the ES Area for correct volume changes 
            
          !! Glacier Water Storage Change
          !! Glacier Accum Correction
          if (glacc < 0.) glacc = 0.
          if (glmlt < 0.) glmlt = 0.
          if (glmlt > hru_glww2(j)) glmlt = hru_glww2(j)        
          !if (glmlt*hru_fr_es(j) > hru_glww(j)) glmlt = hru_glww(j)
          hru_glww(j) = hru_glww(j) + ((glacc - glmlt)*hru_fr_es(j)) !! Timo Glacier Storage Change HRU Specific per ES
          hru_glww2(j) = hru_glww2(j) + glacc - glmlt !! Timo Glacier Storage Change Uniform per ES
          if (hru_glww(j)<0) hru_glww(j)=0

          ! snomlt = smfac * (xx2 + log(1 + exp(-(xx2))))
          ! snomlt2 = snomlt2 * snocov
          precipday = precipday + snomlt + glmlt          
          if (nstep > 0) then
            do ii = 1, nstep
                precipdt(ii+1) = precipdt(ii+1) + snomlt + glmlt / nstep
            end do
          end if
          if (precipday < 0.) precipday = 0.
        end if ! End Of EB If Condition (yes/no)
      return
      end