!  glacier_module_git.f90 
!
!  FUNCTIONS:
!  glacier_module_git - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: glacier_module_git
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

    program glacier_module_git

    implicit none

    ! Variables

    ! Body of glacier_module_git
    print *, 'Hello World'

    end program glacier_module_git

