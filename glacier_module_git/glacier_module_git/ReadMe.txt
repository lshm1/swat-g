========================================================================
    Fortran Console Application : "glacier_module_git" Project Overview
========================================================================

Intel(R) Fortran Console Application Wizard has created this 
"glacier_module_git" project for you as a starting point.

This file contains a summary of what you will find in each of the files 
that make up your project.

glacier_module_git.vfproj
    This is the main project file for Fortran projects generated using an 
    Application Wizard.  It contains information about the version of 
    Intel(R) Fortran that generated the file, and information about the 
    platforms, configurations, and project features selected with the 
    Application Wizard.

glacier_module_git.f90
    This is the main source file for the Fortran Console application. 
    It contains the program entry point.

/////////////////////////////////////////////////////////////////////////////
Other notes:

/////////////////////////////////////////////////////////////////////////////
