# <img title="" src="../images/sgl_logo.png" alt="" width="82">

# SWAT-GL Documentation

This document contains information on how to prepare the inputs to execute SWAT-GL and which concepts are implemented. For further theoretical insights of the $\Delta$h-Parameterization we refer to: [Huss et al. 2010](https://hess.copernicus.org/articles/14/815/2010/) and [Huss et al. 2008](https://onlinelibrary.wiley.com/doi/10.1002/hyp.7055). The documentation will be updated with a summary of all concepts such as the $\Delta$h-approach or the different modifications of the snow routine. However, all concepts are supplemented with the corresponding references, which are recommended to read before application. <br/>More technical details and background infos can be obtained from our just recently published Technical Note: [Schaffhauser et al. 2024](https://onlinelibrary.wiley.com/doi/10.1111/1752-1688.13199). 
A publication covering an application in 4 contrasting glacier basins is available in HESS (preprint): [Schaffhauser et al. 2024b](https://hess.copernicus.org/preprints/hess-2024-89/).

***

# 1) General Overview: Requirements

## Demo Model

A test model from the Martelltal can be obtained from [Zenodo](https://zenodo.org/records/10962425) and used to get an idea on how to prepare everything. 

## Quickstart

Users **need to make use of SWAT's Elevation Bands** (EB) in order to let SWAT-GL work properly and get maximum benefit of the module! 

<img title="" src="../images/workflow.png" alt="" width="609">

## Required Input Data for Model Setup

1. Glacier Outlines
2. Glacier Thickness 

Both datasets are needed for the initialization of SWAT-GL. Glacier outlines for example often stem from the [Randolph Glacier 
Inventory](https://www.glims.org/RGI/), from which one can infer glacier areas. The second information, glacier thicknesses, can nowadays for example be derived from [Farinotti et al. 2019](https://www.nature.com/articles/s41561-019-0300-3) or [Millan et al. 2022](https://www.nature.com/articles/s41561-021-00885-z). 

## New Input Files to prepare

1. *swat_gles_full.txt*
2. *gl_hru_par.txt*
3. *swatgl_control.txt*

File names are hard-coded and thus must be used as written above. One example file for each can be found in the documentation folder.
<br/><br/> 
The files can be described as follows: <br/>
**_swatgl_control.txt_** includes the information of the general SWAT-GL snow and glacier-specific setup. In detail, 13 parameters are provided, of which 5 are related to the choice of methods and submodules used while the remaining 8 parameters represent calibration parameters. The calibration parameters refer all to the adapted snow routine and are explained further in 3). However, they are related to the newly introduced methods ETI (Enhanced Temperature-Index, [Pelliccotti et al. 2005](https://www.cambridge.org/core/journals/journal-of-glaciology/article/an-enhanced-temperatureindex-glacier-melt-model-including-the-shortwave-radiation-balance-development-and-testing-for-haut-glacier-darolla-switzerland/E96A8B8D2903523DE6DBF88E2E06E6D9)), HTI (Hock Temperature Index, [Hock 1999](https://www.cambridge.org/core/journals/journal-of-glaciology/article/distributed-temperatureindex-ice-and-snowmelt-model-including-potential-direct-solar-radiation/2013F2D6B911401D67EA84728AF93629)), a ROS (Rain on Snow) correction, as well as precipitation adaption allowing for mixed precipitation (solid and liquid) and an exponential TI method (both [Magnusson et al. 2014](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2014WR015302)). The file defines the following parameters: a parameter to select the snow melt model (SM_mod), a decision flag to allow for additional melt for rain on snow events (RoS_fl), a parameter to select how snow cover is linked with the glacier routine (GM_Init), a parameter that sets whether the degree day factor for ice is automatically corrected if it is smaller than the counterpart for snow or not (fgm_cor), the degree day factor for snow for the HTI or ETI, the short wave radiation factor in the ETI (SWRfac), the wet degree dayfactor for ROS events (Pfac) and the threshold rainfall above which additional melt is allowed to occur (PTHR). Besides, there is a flag for mixed precipitation to be allowed (MP_fl), the temperature below which mixed precipitation can occur (tmix) and an adjustment factor for the exponential snow melt module (Exp_fac). <br/>
The **_gl_hru_par.txt_** contains the HRU-specific parameter information of the glacier routine. In detail, 5 new parameters are here defined for all glaciated HRUs. The file consists of 6 columns, namely the HRU ID, the glacier melt temperature (GLMTMP), the maximum glacier melt factor on June 21st (GLMFMX), the minimum glacier melt factor on June 21st (GLMFMN), the refreezing rate of melt (f_frze), as well as the transfer rate of snow to ice (f_accu). Those parameters are simultaneously the ones which are introduced for calibration in SWAT-GL.<br/> The example file demonstrates the format and is provided in the Documentation folder. The file consists of 17 HRUs, where 5 HRUs (HRU #13-17) are glaciated. All other HRUs are non-glaciated, in the _gl_hru_par.txt_ these HRUs are indicated as 0-lines. In detail, the file covers all HRUs, however only glaciated HRUs are parameterized, the remaining HRUs are represented by six 0 values. <br/> 
The **_swat_gles_full.txt_** is used for the initialization of SWAT-GL. The file is used to set the initial glacier area, thickness and volume throughout the basin. In detail, it contains the glacier information on the subbasin and ES scale. Glaciers are initialized on the subbasin scale, means that all glaciated areas within a subbasin correspond to a SWAT-specific glacier. Further, for each subbasin we define a glacier area, volume and ice thickness individually for all ES. The file only covers glaciated subbasins and for each glaciated subbasin all ES must be defined. This means not only ES that actually are present in the subbasin, but also those who are not glaciated.<br/> If we assume a model with 20 ES on the basin scale an 3 subbasins that are glaciated the file would consist of 20 lines for each subbasin, making 60 lines which results in 61 lines including the header line. 
The example file demonstrates a basin with 2 glaciated subbasins (ID 10 & 12) and 10 ES. Subbasin 10 comprises 3 ES (ES #3-5) and subbasin 12 covers 6 ES (ES #2-7). The remaining ES lines obtain 0 values. One important note is that the ice thickness must be provided in milimeter water equivalent [mm H20], area in square kilometers [km²] and volume is provided in cubic kilometers [km³]. For the conversion of ice to water equivalent a density of 917 kg/m³ is assumed.

## New & Modified Output Files

1. _output.hru_ was modified and now contains daily mass balance calculations <br/>
2. *gl_mb_aa.txt* is newly created and contains the annual glacier evolution estimates

At the moment the output.sub is not supported due to some general issues with the outputs provided in the file in SWAT. 

## Hourly Representation

Under progress and updated soon.

****

# 2) Preprocessing

## Define Elevation Sections (ES)

As a first step, one has to discretize the glaciers within the basin boundary in different sections. A basin-wide spacing should be defined, e.g. 100 m. The spacing will then be used to split your glaciers in N sections, which we call Elevation Sections (ES). The spacing becomes important for the initialization of the glacier evolution routine.  <br/> <br/> 
For instance, if your minimum glacier elevation in the basin is at 2000 m and the maximum elevation of your glacier is at 3000 m, a spacing of 100 m results in N=10 ES. This is important, as glaciers in SWAT-GL are defined on the subbasin scale, all subbasins have exactly the same amount of ES in the initialization file, although one subbasin might only have one actual glaciated ES. 

## Land Use Map

Next, the land use map has to be modified according to the defined spacing. If your land use map contains already a land use class "glacier", this is the class you have to split into the N land use classes, one for each ES. If you are not satisfied with your (default/original) land use class glacier (e.g. too coarse, outdated etc.), here would be a good point to consider a glacier inventory to reset the general glacier extent within your basin. 
<br/><br/> 
Apparently, for that step you need a DEM in order to split the glacier land use class into elevation-dependent sections. 
Each newly defined land use class needs a unique ID in the land use map to be distinguished by SWAT later on. For example, if your original land use map had the ID "200" for glacier you could use the IDs "201" up to "210", if we assume again an ES number of 10.
<br/> 
See the following example:<br/>
<img src="../images/martell_lulc.png" width="55%" /> 

<figcaption>Fig. 1: Original Land Use Map</figcaption>

<img src="../images/martell_lulc_es.png" width="55%" title="Logo title"/> 

<figcaption>Fig. 2: Modified Land Use Map with 10 new classes, 1 per ES</figcaption>

Here we created exactly 10 new land use classes (341 to 350).

##### Land Use Map Preperation - QGIS

One example solution in QGIS is exemplified in the following. However, there are plenty of ways, potentially even simpler.

You have a land use map called "lulc" with a glacier land use class and a DEM called "dem":

- Raster/Conversion/Polygonize lulc -> lulc2

- Dissolve lulc2 -> lulc2b

- Extract glacier land use class polygons -> lulc_gl

- Raster/Extraction/ Clip Raster by Mask Layer DEM with lulc_gl (uncheck "match to extent of mask layer") -> dem_gl

- Reclassify by Table dem_gl split in ES -> dem_gl_reclas (what you see in Fig. 2)

- r.patch merge dem_gl_reclas & lulc (original) -> lulc_final

## Crop Database

According to the newly defined ES land use classes, you have to define one new entry in the crop database within your SWAT project database. This is crucial, as SWAT-G internally uses the land use class name for several calculations, to properly apply the $\Delta$h-Parameterization and to distinguish between HRUs, as they are assigned to a specific ES through the land use class name.

If we follow our example with the 10 ESs, 10 new land use class entries have to be defined. All entries receive the same properties. How you parameterize your glacier land use classes is basically up to you. However, we <u>don't</u> recommend to use the "WATR" class, which is often used and also sometimes recommended to use for glaciers. A starting point might be to use the properties of bare soil. 

**Important**: LU class entries in the crop databse need to be called ES1, ES2,...,ESx as shown in the image below and then properly assigned with the LU map through your lookup table!

![](../images/crop_database.png)

<figcaption>Fig. 3: Example crop database, each ES needs a unique entry named as ES1, ES2 and so on.</figcaption>

## File Preperation

To prepare the 3 new and required input files you need to setup the SWAT model as usual (e.g. ArcSWAT) as we need the subbasins and HRUs to initialize them with glacier information:

* *swatgl_control.txt* can directly be set up without the actual SWAT model

* *gl_hru_par.txt* HRUs must be defined through SWAT model set up so that you can parameterize all glacier HRUs

* *swat_gles_full.txt* Subbasin must be defined through SWAT model setup so that you can initialize glacier area and volume per subbasin and ES

For the initialization of the subbasin-specific ES and the glacier HRUs the SWAT model setup must be finished already, considering the land use changes and crop database adaptions described before. 

### Subbasin Initialization: *swat_gles_full.txt*

When the subbasin delineation is completed, you can start the initialization of the ES per subbasin (as in Fig. 2). Based on the land use infos of each subbasin, you can see which ES are located in which subbasin. You can just check the *LandUseSoilsReport.txt* in the *\Watershed\Text* folder of your SWAT project. Logically, for the initialization of the ES you need to know the ice thickness of your ES. As a starting point the data of Farinotti et al. mentioned earlier could be used. You then have to find the mean thickness of each ES and convert it to water equivalent. Ice density of ice is usually assumed to be approx. 917 kg/m³. Analogously to snow water equivalen, which is abbreviated as SWE, we just call the water equivalent from glaciers GWE.   

You then create the *swat_gles_full.txt* file, where you set the glacier area and initial ice thickness for each subbasin and ES. An example is provided in Fig. 4 for a model that contains two glacierized subbasins and for which we defined ES with 50 m intervalls. 

<img title="" src="../images/swat_gles_full_txt.png" alt="" width="590">

<figcaption>Fig. 4: swatgl_control.txt file.</figcaption>

Figure 4 shows an example with two subbasins (subbasin 1 and subbasin 4) and 10 ESs.  The model can have for example 5 subbasins in total, but only subbasin 1 and 4 contain glaciers, as shown here. You have to provide one line per ES for every glaciated subbasin, regardless whether the ES is actually present in the subbasin. In this example you can see that in subbasin 1 only ES 2 to 10 are present, ES 1 on the other hand is not present. It thus just receives a line of 0 values. In subbasin 4, ES1 to 3 are not present (again 0 value lines). A couple of additional columns are provided for the SWAT-GL initialization. You can ignore the *Rel_A* and *Rel_V* columns, they are not used and side information, you can put 0 values or other dummy values here. 

The colums in detail are:

* *A_km2*: Area in km² of the ES in that subbasin; Obtained from either your own GIS, Python or whatever calculation or you just use the values from the text files in your SWAT model, e.g.: *HRULandUseSoilsReport.txt* (do not forget to convert ha to km²)

* *GWE_mm*: Water equivalent of ice in mm (mean ice thickness of the ES converted to water equivalent)

* *V_km3*: Volume of ice in that ES (not as water equivalent) in km³. Should be Area multiplied with GWE (after unit conversion logically and considering density of ice). 

* *Rel_A*: Relative ES area in subbasin (ES Fraction [-]) with respect to total <u>glacierized</u>subbasin area.

* *Rel_V*: Relative ice volume of ES in subbasin (Fraction [-]) with respect to total subbasin ice volume.

* *W_GWE_mm*: Weighted GWE (*Rel_A x GWE*) based on areal ES fraction in mm. 

* *ES_UL_m:* Upper Elevation limit of the ES, which is used to calculate the ES spacing based on 2 adjacent ESs. Spacing should be regular and not change. 

### Subbasin Initialization: *swatgl_control.txt*

This file was not existing in [Schaffhauser et al. 2024](https://onlinelibrary.wiley.com/doi/10.1111/1752-1688.13199) and is thus not found in the file list within this publication. It now serves as a setup file to select the concepts that should be used in SWAT-GL. This applies at the moment especially to the snow melt calculations. The file looks as follows: 

![](../images/swatgl_control.txt.png)

<figcaption>Fig. 5: swatgl_control.txt file.</figcaption>

We have a couple of setup flags and a couple of parameters:

* *SM_mod*: Defines snow melt routine used; 0 = Standard SWAT / 1 = HTI / 2 = ETI / 3 = Exp. TI 

* *RoS_fl*: Rain on snow flag; 0 = not considered (standard SWAT) / 1 = Rain on snow melt correction

* *Tfac*: Temperature degree day factor [mm/(d*C°)]; Only used for HTI or ETI (SM_mod = 1 or 2). If the melt factor from SWAT standard wants to be use a value of 0 here (then the sinusoidal factor that accounts for daily variations is used).   

* *radfac_s*: Radiation factor for snow [mm m²/(d *W *°C)]; Only used for HTI (SM_mod = 1)

* *radfac_i*: Radiation factor for ice [mm m²/(d *W *°C)]; Only used for HTI (SM_mod = 1)

* *SWRfac*: Shortwave radiation factor [mm m²/(d*W)]; Only used for ETI (SM_mod = 2)

* *Pfac*: Melt factor for rain on snow events [mm/(mm precip.)]; Only used if rain on snow active (RoS_fl = 1) 

* *PTHR*: Rainfall threshold height at which additional melt occurs [mm/d]; Only used if rain on snow active (RoS_fl = 1)

* *GM_Init*: Flag that controls how snow cover is considered to let glacier melt occur; 0 = compares ES and snow cover / 1 = Mean subbasin snow cover decisive (can have significant effects on glacier melt) 

* *fgm_cor*: Correction of glacier melt degree day factor if < than snow melt degree day factor / 0 = no; 1 = yes (higher physical plausibility)

* *MP_fl*: Flag to allow for mixed precipitation; 0 = no (standard SWAT); 1 = yes (Snowfall and rainfall possible in parallel)

* *tmix*: Base temperature below which mixed precipitation is possible [°C]

* *Exp_fac*: Adjustment for exponential snow melt model [-]; Only used for exp. TI (SM_mod = 3)

It is highly recommended to read the provided original literature for application of alternative modules as for example for snow melt calculations. 

### Subbasin Initialization: *gl_hru_par.txt*

Here you determine the parameters for all glacier HRUs. The file can be prepared just after the HRU definition step in the standard model setup of SWAT. How do you receive the infos required to create the file? Again e.g. go to the *\Watershed\Text* folder of your SWAT project and check the HRULandUseSoilsReport.txt to get a list of all HRUs. All HRUs that have a land use class "glacier" (indicated as ESXX) are your glacier HRUs. Now, create a file that looks as follows:

![](../images/gl_hru_par.txt.png)

<figcaption>Fig. 6: gl_hru_par.txt file.</figcaption>

The file has one row for each HRU, regardless whether glaciated or not, e.g. if your model has 100 HRUs the file contains 100 lines (+ 1 header line as seen in Fig. 6). Non-glacier HRU are indicated as 0 lines, while glaciated HRUs accordingly receive the desired parameter values. In Fig. 6 we would have 25 HRUs, of which 17 HRUs are glacier HRUs and 8 are non HRUs (rows full of 0s). 

The columns refer to HRU ID and 5 parameters:

* *HRU*: HRU number

* *GLMTMP*: Glacier melt temperature [°C]

* *GLMFMX*: Maximum melt factor on June 21 [mm H2O/(°C*d)]

* *GLMFMN*: Minimum melt factor on December 21 [mm H2O/(°C*d)]

* *f_frze*: Refreezing factor [-]

* *f_accu*: Accumulation factor [-]

## Model Run

When everything is prepared just run the executable found in Gitlab under *swat-g/src/gl_routine* and called *glacier_module_git_release.exe*

Just put the *.exe* in your TxtInOut folder to run SWAT-GL. 

## Example

In the following we want to briefly give an idea of the subbasin-specific glacier initialization and the preperation of the _swat_gles_full.txt_ file. We can see the ice thickness distribution within the subbasin in combination with the contour lines of the individual ES. In the example the ES spacing is 50 m. One would now have to calculate the area, the mean ice thickness and the ice volume for each ES (indicated with the contour lines). Then everything should be converted to water equivalent and the units must be adjusted to be mm for GWE (water equivalent of ice), km³ for the ice volume of that ES (expressed as water equivalent) and km² for the area of that ES. 

<img src="../images/icethi_example.png" width="45%" title="Logo title"/> 

<figcaption>Fig. 7: Ice thickness distribution within one example subbasin.</figcaption>

***

# 3) Post-Processing/Run

After the model run you find new columns in the *output.hru* and a new file called *gl_mb_aa.txt*. 

![](../images/gl_mb_aa.png)

<figcaption>Fig. 8: gl_mb_aa.txt file.</figcaption>

The file contains annual mass balance summaries. Annual refers to the glaciological year. Provided are information on the subbasin size, the net mass balance in m³ (*dM_m3*) where negative values indicating ablation and positive values accumulation, the new water equivalent of ice for each ES in mm (*GWE_mm*) and the (glacier) area of each ES in km² (*Agl_km2*). Net mass balance change refer to the total change on the subbasin scale rather than the ES scale!

The example in Figure 8 demonstrates the results of year 1974 with a negative mass balance (ablation) for 2 subbasins (subbasin 1 and 4) with 10 ES. When an ES is ice-free it is indicated here by a 0 value for *GWE_mm*. The file contains the information of each year for all glacierized subbasins. Basin-wide information are not provided yet and must be aggregated by the user. You can just sum the *dM_m3* infos of each year of all subbasins for example to get total basin mass balance rates.    

# 4) Theoretical Documentation

## a) Snow Processes

SWAT-GL does not only aim to include glacier processes, but also intends to collect and implement existing approaches to extend SWAT's snow routine.
We thus incorporated the following approaches for snow melt modelling in addition to default Temperature Index model (TI): <br/>

1. Potential Radiation Model (HTI) from _Hock 1999_
2. Enhanced Temperature Index Model (ETI) from _Pelliccotti et al. 2005_
3. Rain on Snow Correction (ROS) 
4. Mixed Precipitation from _Magnusson et al. 2014_
5. Exponential Temperature Index (ExpTi) from _Magnusson et al. 2014_

All approaches can be selected via the SM_mod flag in the control file (*swatgl_control.txt*). A value of 0 (default) uses SWAT's standard routine, a value of 1 refers to the HTI, 2 to the ETI model and 3 to the Exponential TI model. <br/>
If users set the RoS_fl to 1 (Default = 0) all melt modules can be subject to ROS corrections, which allows additional melt to occur. <br/>
The GM_Init parameter allows users to choose whether the snow cover fraction is linked to the subbasin glacier cover where the difference between the two is distributed over the ES to limit melt to lower areas. The assumption is the higher parts are more likely to be snow covered. Users can set this option using GM_Init = 1. A less advanced approach that usually produces more glacier melt is by setting the parameter to 0 which just corrects glacier melt by the exposed glacier area regardless whether a HRU is located at a high ES or not. <br/>
fgm_cor = 0 does not correct the degree day factor for ice if it is lower than the one for snow which is physically less plausible. Determining fgm_cor as 1 does the correction internally if desired. <br/>
Setting MP_fl to 1 will allow for a temperature range in which precipitation occurs mixed, as solid and liquid precipitation. 
An example _swatgl_control.txt_ file is provided in the documentation folder of SWAT-GL. 

### General Formulation of Snowmelt in SWAT

$$
M = \left(\frac{T_{mx} + T_{snow}}{2} - T_{smlt}\right) \cdot b_{smlt} \cdot sno_{cov}\\[5pt]
b_{smlt} = \frac{SMFMX + SMFMN}{2} + \left(\frac{SMFMX - SMFMN}{2} \cdot sin\left[\frac{2\pi}{365}(i-81)\right]\right)
$$

### Potential Radiation Model (HTI)

$$
M = \left(\frac{T_{mx} + T_{snow}}{2} - T_{smlt}\right) \cdot b_{tot}\\[5pt]
b_{tot} = b_{smlt} + f_{rad} \cdot I_{pot}
$$

### Enhanced Temperature Index Model (ETI)

$$
M = \left(\frac{T_{mx} + T_{snow}}{2} - T_{smlt}\right) \cdot b_{tot}\\[5pt]
b_{tot} = b_{smlt} + f_{swr} (1-\alpha) \cdot G
$$

### Exponential Temperature Index (ExpTI)

$$
M = b_{smlt} \cdot m_m(T_{m} + ln(1+e^{-T_m})) \\[5pt]
T_m = \frac{T_{mx}-T_{smlt}}{m_m}
$$

### Rain on Snow (RoS)

$$
M = \left(\frac{T_{mx} + T_{snow}}{2} - T_{smlt}\right) \cdot b_{tot}\\[5pt]
b_{tot} = b_{wet} + b_{smlt}\\[5pt]
b_{wet} = \begin{cases}
b_{wd}(P_t - P_{thr}), & \text{if } P_t>P_{thr}\\
0, & \text{if } P_t \leq P_{thr}\end{cases}
$$

### Mixed Precipitation

$$
SF =  
\begin{cases} 
P, & T_{d} \leq T_{sf}\\
\frac{P}{1 + e^{T_p}}, &  T_{d} > T_{sf} \text{ and } T_{d} \leq T_{mix}
\end{cases} \\[5pt]
T_p = \frac{T_{d}-T_{sf}}{T_{mix} - T_{sf}}
$$

## b) Glacier Processes

Coming soon
